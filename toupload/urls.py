from django.urls import path
from toupload.views import upload_file, show_uploaded_file, show_home, uploadFileCsv, uploadFileExcel

urlpatterns = [
  path('', show_home, name='home'),
  path('upload/', upload_file, name='upload'),
  path('mostrar/', show_uploaded_file, name='mostrar'),
  path('cargar_csv/', uploadFileCsv, name='cargar_csv'),
  path('cargar_excel/', uploadFileExcel, name='cargar_excel')

]
