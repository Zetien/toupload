from django.db import models

# Create your models here.
class Read_excel(models.Model):
    region = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    item_type = models.CharField(max_length=100)
    sales_channel = models.CharField(max_length=50, null=True)
    order_priority = models.CharField(max_length=5)
    order_date = models.DateField()
    order_id = models.IntegerField()
    ship_date = models.DateField()
    units_sold = models.IntegerField()
    unit_price = models.FloatField()
    unit_cost = models.FloatField()
    total_revenue = models.FloatField()
    total_cost = models.FloatField()
    total_profit = models.FloatField()
    
class UploadFile(models.Model):
    uploadedFile = models.FileField(upload_to='uploads/csv/')
    title=models.CharField(max_length=150)
    date_time_of_upload=models.DateTimeField(auto_now=True)

class Read_csv(models.Model):
    date_time = models.DateField()
    description = models.CharField(max_length=150)
    deposist = models.FloatField()
    withdrawls = models.FloatField()
    balance = models.FloatField()



