from xml.etree.ElementTree import fromstringlist
from django.contrib import admin
from toupload.models import Read_excel, UploadFile, Read_csv

# Register your models here.

admin.site.register(Read_excel)
admin.site.register(UploadFile)
admin.site.register(Read_csv)

