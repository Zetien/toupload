import csv
import pathlib
from datetime import datetime

import pandas as pd
from django.shortcuts import render
from openpyxl.worksheet._reader import WorksheetReader
from utils.utils import format_date, clean_float, read_excel
from . import models
from .models import Read_csv, Read_excel


# Create your views here.

def upload_file(request):
    msg = ''
    documents = models.UploadFile.objects.all()
    if request.method == 'POST':
        ext = ['.csv', '.xls', '.xlsx']
        file_title = request.POST["fileTitle"]
        upload_files = request.FILES['uploadedFile']
        # text.csv el csv es el sufijo del archivo
        extension = pathlib.Path(upload_files.name).suffix

        if extension in ext:
            document = models.UploadFile(
                title=file_title,
                uploadedFile=upload_files,

            )

            document.save()
            documents = models.UploadFile.objects.all()  # SELECT * FROM documents
            """ data = []
            if document:
                with open(document.uploadedFile.path) as file:
                    content = csv.DictReader(file) #lee el archivo y lo convierte en una lista de diccionarios
                    for row in content:
                        data.append(row)
                for row in data:
                    upload = Upload(
                        region = row[''],
                        country = row[]
                        item_type = 
                        sales_channel = 
                        order_priority = 
                        order_date = 
                        order_id = 
                        ship_date = 
                        units_sold = 
                        unit_price = 
                        unit_cost = 
                        total_revenue = 
                        total_cost = 
                        total_profit = 
                    )
                    upload.save() """

        else:
            document = None
            msg = 'Solo se permiten archivos con extensiones .csv, .xls, .xlsx'
    return render(request, "upload_file.html", context={'files': documents, 'msg': msg})


def show_uploaded_file(request):
    return render(request, "show_uploaded_file.html")


def show_home(request):
    return render(request, "index.html")


def uploadFileCsv(request):
    data = None
    msg = ''
    documents = None
    context = None
    if request.method == "POST":
        ext = ['.csv']
        file_title = request.POST["fileTitle"]
        upload_files = request.FILES['uploadedFile']

        extension = pathlib.Path(upload_files.name).suffix

        if extension in ext:
            document = models.UploadFile(
                title=file_title,
                uploadedFile=upload_files
            )
            document.save()

            data = read_filecsv(document)
            imported_data = pd.DataFrame(data)
            count_description = imported_data['description']
            count_description = count_description.value_counts()  # lista y enumera diferentes valores de descripcion
            imported_data = imported_data.drop(columns=['description', 'deposist', 'balance'])
            imported_data = imported_data.rename(columns={'withdrawls': 'Transacciones'})
            count_years = imported_data.groupby(
                pd.Grouper(key='date_time', axis=0, freq='Y')).count()  # register per year
            count_years = count_years.to_html(border=0)
            count_descriptions = count_description.to_frame()
            count_descriptions = count_descriptions.to_html(border=0)
            print(count_years)
            context = {'files': data,
                       'msg': msg,
                       'count_row': len(imported_data.index),  # cuenta las filas guardadas
                       'count_description': count_descriptions,
                       'count_years': count_years,
                       }
        else:
            msg = 'Archivo No Valido'
    return render(request, "cargar_csv.html", context=context)


def read_filecsv(document):
    data = []
    if document:
        with open(document.uploadedFile.path) as file:
            content = csv.reader(file)  # lee el archivo y lo convierte en una lista de diccionarios
            next(content)
            columns = ['date_time', 'description', 'deposist', 'withdrawls', 'balance']

            for row in content:
                date = datetime(int(row[0].split("-")[2]), format_date(row[0].split("-")[1]), int(row[0].split("-")[0]))
                new_row = [date, row[1], clean_float(row[2]), clean_float(row[3]), clean_float(row[4])]
                print(zip(columns, new_row))
                obj = dict(zip(columns, new_row))
                data.append(obj)

        for row in data:
            Read_csv(**row).save()

    return data


def uploadFileExcel(request):
    data_excel = None
    msg = ''
    documents = None
    context = None
    if request.method == "POST":
        ext = ['.xls', '.xlsx']
        file_title = request.POST["fileTitle"]
        upload_files = request.FILES['uploadedFile']

        extension = pathlib.Path(upload_files.name).suffix

        if extension in ext:
            document = models.UploadFile(
                title=file_title,
                uploadedFile=upload_files
            )
            document.save()

            data = read_xlsx(document)
            imported_data= pd.DataFrame(data)
            count_data=len(imported_data)  # count rows

            count_itemtype = imported_data['Item Type']  # get dataframe values
            count_itemtype = count_itemtype.value_counts()  # get counted values
            count_itemtype = count_itemtype.to_frame().to_html


            count_region = imported_data['Region']  # get dataframe values
            count_region = count_region.value_counts()  # get counted values
            count_region = count_region.to_frame().to_html  # get key list
           
            #  hacer igual que en las lineas 159-161

            count_priority = imported_data['Order Priority']
            count_priority = count_priority.value_counts()
            count_priority = count_priority.to_frame().to_html  # get key list
            
            #  hacer igual que en las lineas 159-161

            print(count_priority)
            context = {'files': data.values.tolist(),
                       'msg': msg,
                       'count_data': count_data,
                       'count_description':count_itemtype,

                       'count_region': count_region,

                       'count_priority': count_priority,
                       }
        else:
            msg = 'Archivo No Valido'
    return render(request, "cargar_excel.html", context=context)


def read_xlsx(document):
    content = None
    if document:
        content = pd.read_excel(
            document.uploadedFile.path)  # lee el archivo y lo convierte en una lista de diccionarios
        columns = ['Region', 'Country', 'Item Type', 'Sales Channel', 'Order Priority', "Order Date", 'Order ID',
                   'Ship Date',
                   'Units Sold', 'Unit Price', 'Unit Cost', 'Total Revenue', 'Total Cost', 'Total Profit']
        for data in columns:  # add the column to the object
            if data == "Order Date" or data == "Ship Date":  # reformatea las filas de fecha
                date = pd.to_datetime(content[data]).apply(lambda x: x.date())  # columna reformada
                content[data] = date
        for index, row in content.iterrows():
            Read_excel.objects.create(
                region=row['Region'],
                country=row['Country'],
                item_type=row['Item Type'],
                sales_channel=row['Sales Channel'],
                order_priority=row['Order Priority'],
                order_date=row['Order Date'],
                order_id=row['Order ID'],
                ship_date=row['Ship Date'],
                units_sold=row['Units Sold'],
                unit_price=row['Unit Price'],
                unit_cost=row['Unit Cost'],
                total_revenue=row['Total Revenue'],
                total_cost=row['Total Cost'],
                total_profit=row['Total Profit'],
            ).save()

    return content
