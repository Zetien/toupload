import pandas as pd


def format_date(mes):
    if mes.lower()=='jan':
        return 1
    elif str(mes).lower()=='feb':
        return 2
    elif str(mes).lower()=='mar':
        return 3
    elif str(mes).lower()=='apr':
        return 4
    elif str(mes).lower()=='may':
        return 5
    elif str(mes).lower()=='jun':
        return 6
    elif str(mes).lower()=='jul':
        return 7
    elif str(mes).lower()=='aug':
        return 8
    elif str(mes).lower()=='sep':
        return 9
    elif str(mes).lower()=='oct':
        return 10
    elif str(mes).lower()=='nov':
        return 11
    elif str(mes).lower()=='dec':
        return 12
    else:
        return mes

def clean_float(lim):
    return float(lim.replace(',',''))

def read_excel(path):
    df = pd.read_excel(io=path)
    return df